# dotenv

Add to `index.php` file of your project
```php
use miks1108\dotenv\Dotenv;

Dotenv::load(__DIR__); // where __DIR__ is path to directory with .env file
```

Using in config

```php
'db' => array_merge(
    ['class' => Connection::class],
    Dotenv::getConfig([
        'dsn' => 'DB_DSN',
        'username' => 'DB_USERNAME',
        'password' => 'DB_PASSWORD',
        'charset' => 'DB_CHARSET'
    ])
)
```
