<?php

namespace miks1108\dotenv;

use Symfony\Component\Dotenv\Dotenv as SymfonyDotenv;

/**
 * Class Dotenv
 */
class Dotenv
{
    /**
     * @var string
     */
    const ENV_FILENAME = '.env';

    /**
     * @param string $path
     * @param string $filename
     *
     * @reurn void
     */
    public static function load(string $path, string $filename = self::ENV_FILENAME): void
    {
        $dotenv = new SymfonyDotenv();
        $dotenv->load(self::getFilePath($path, $filename));
    }

    /**
     * @param string $param
     * @param mixed $defaultValue
     *
     * @return mixed
     */
    public static function getEnv(string $param, mixed $defaultValue = null): mixed
    {
        return $_ENV[$param] ?? $defaultValue;
    }

    /**
     * ```php
     * ['host' => 'SERVER_HOST'] = ['host' => '127.0.0.1']
     * ```
     *
     * @param array $params [configKey => envParam]
     * @param mixed $defaultValue
     *
     * @return array
     */
    public static function getConfig(array $params, mixed $defaultValue = null): array
    {
        $result = [];

        foreach ($params as $configKey => $envParam) {
            if (is_array($envParam)) {
                $result[$configKey] = self::getConfig($envParam);
            } else {
                $result[$configKey] = self::getEnv($envParam, $defaultValue);
            }
        }

        return $result;
    }

    /**
     * @param string $path
     * @param string $filename
     *
     * @return string
     */
    private static function getFilePath(string $path, string $filename): string
    {
        return "$path/$filename";
    }
}
